import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './pages.routing';
import { HomeComponent } from './home/home.component';
import { RankingComponent } from './ranking/ranking.component';
import { StartGameComponent } from './start-game/start-game.component';
import { PlayGameComponent } from './play-game/play-game.component';
import { PartialsModule } from '../partials/partials.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    routing,
    PartialsModule,
    FormsModule
  ],
  declarations: [HomeComponent, RankingComponent, StartGameComponent, PlayGameComponent]
})
export class PagesModule { }
