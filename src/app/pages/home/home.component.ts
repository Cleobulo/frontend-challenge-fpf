import { Component, OnInit } from '@angular/core';
import { RankingService } from 'src/app/core/services/ranking.service';
import { Player } from 'src/app/core/models/player';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private allPlayers: Player[];

  constructor(
    private rankingService: RankingService
  ) { }

  ngOnInit() {
    this.rankingService.getAllPlayers().subscribe(
      json => {
        if (json) {
          this.allPlayers = json;
        }
      },
      err => {
        console.error(err);
      },
      () => {
        this.allPlayers = this.rankingService.sortPlayersList(this.allPlayers);
        this.rankingService.savePlayersOnCache(this.allPlayers);
      }
    );
  }

}
