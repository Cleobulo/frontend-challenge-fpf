import { Player } from './../../core/models/player';
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Router } from '@angular/router';
import { StartGameService } from 'src/app/core/services/start-game.service';

@Component({
  selector: 'app-start-game',
  templateUrl: './start-game.component.html',
  styleUrls: ['./start-game.component.css']
})
export class StartGameComponent implements OnInit {

  constructor(
    private routes: Router,
    private startGameService: StartGameService
  ) { }

  ngOnInit() {
  }

  onClickNavigateToGame() {
    this.routes.navigate(['play']);
  }

}
