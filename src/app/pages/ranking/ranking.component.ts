import { Component, OnInit } from '@angular/core';
import { RankingService } from 'src/app/core/services/ranking.service';
import { Player } from 'src/app/core/models/player';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {

  public allPlayers: Player[] = [];

  constructor(
    private rankingService: RankingService
  ) { }

  ngOnInit() {
    if (this.rankingService.retrievePlayersFromCache()) {
      this.allPlayers = this.rankingService.retrievePlayersFromCache();
    } else {
      this.rankingService.getAllPlayers().subscribe(
        json => {
          if (json) {
            this.allPlayers = json;
          }
        },
        err => {
          console.error(err);
        },
        () => {
          this.allPlayers = this.rankingService.sortPlayersList(this.allPlayers);
          this.rankingService.savePlayersOnCache(this.allPlayers);
        }
      );
    }
  }

}
