import { StartGameComponent } from './start-game/start-game.component';
import { HomeComponent } from './home/home.component';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RankingComponent } from './ranking/ranking.component';
import { PlayGameComponent } from './play-game/play-game.component';

export const pagesRoutes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'ranking', component: RankingComponent },
    { path: 'start', component: StartGameComponent },
    { path: 'play', component: PlayGameComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(pagesRoutes);
