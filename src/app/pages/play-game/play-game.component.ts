import { Component, OnInit, ChangeDetectorRef, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { StartGameService } from 'src/app/core/services/start-game.service';
import { RankingService } from 'src/app/core/services/ranking.service';
import { Router } from '@angular/router';

import 'jquery';
import { Player } from 'src/app/core/models/player';

declare var $: any;

@Component({
  selector: 'app-play-game',
  templateUrl: './play-game.component.html',
  styleUrls: ['./play-game.component.css']
})
export class PlayGameComponent implements OnInit, OnDestroy {

  public jogadorHP = 100;
  public monstroHP = 100;
  public countTurnosJogador = 2;
  public countTurnosMonstro = 2;
  public habilitarCura = true;
  public playerTime = true;
  public logs: any[] = [];
  public playerName = '';
  private jogadorFinalScore = 0;
  private monstroFinalScore = 0;

  @ViewChild('playGame') private inputPlayGame: ElementRef;


  constructor(
    private startGameService: StartGameService,
    private rankingService: RankingService,
    private changeReference: ChangeDetectorRef,
    private routes: Router
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.onClickMonstroAtaca();
    }, 1000);
  }

  ngOnDestroy() {
    this.changeReference.detach();
  }

  public onClickAtacar() {
    const less = Math.floor((Math.random() * 8) + 5);

    if (less >= 0 && this.monstroHP >= 0) {
      this.monstroHP -= less;
      this.jogadorFinalScore += less;
      this.logs.push({
        isJogador: true,
        menssagem: `Jogador atacou o monstro (-${less})`,
        cor: `#192a56`
      });

      if (this.monstroHP <= 0) {
        $('#resultModal').modal('show');
      }
    }
    if (this.countTurnosJogador > 0) {
      this.countTurnosJogador -= 1;
    }
    if (this.jogadorHP > 0 && this.monstroHP > 0) {
      setTimeout(() => {
        this.onClickMonstroAtaca();
      }, 1500);
    }

    this.changeReference.detectChanges();
  }

  public onClickAtaqueEspecial() {
    const less = Math.floor((Math.random() * 11) + 7);

    if (less >= 0) {
      this.monstroHP -= less;
      this.jogadorFinalScore += less;
      this.logs.push({
        isJogador: true,
        menssagem: `Jogador usou golpe especial (-${less})`,
        cor: `#c23616`
      });
      this.countTurnosJogador = 2;

      if (this.monstroHP <= 0) {
        $('#resultModal').modal('show');
      }
    }
    if (Math.floor((Math.random() * 100)) <= 25) {
      this.habilitarCura = false;
    }
    if (this.jogadorHP > 0 && this.monstroHP > 0) {
      if (Math.floor((Math.random() * 100)) > 40) {
        setTimeout(() => {
          this.onClickMonstroAtaca();
        }, 1500);
      }
    }

    this.changeReference.detectChanges();
  }

  public onClickCurar() {
    const plus = Math.floor((Math.random() * 10) + 5);

    if (this.jogadorHP <= 90) {
      this.jogadorHP += plus;
      this.logs.push({
        isJogador: true,
        menssagem: `Jogador usou a cura (+${plus})`,
        cor: `#44bd32`
      });
      this.habilitarCura = true;
    }

    this.changeReference.detectChanges();
  }

  public onClickDesistir() {
    this.routes.navigate(['start']);
  }

  private onClickMonstroAtaca() {
    const less = Math.floor((Math.random() * 12) + 6);

    if (less >= 0 && this.jogadorHP > 0) {
      this.jogadorHP -= less;
      this.monstroFinalScore += less;
      this.logs.push({
        isJogador: false,
        menssagem: `Monstro causou dano (-${less})`,
        cor: `#e55039`
      });

      if (this.jogadorHP <= 0) {
        $('#resultModal').modal('show');
      }
    }
    if (this.countTurnosMonstro > 0) {
      this.countTurnosMonstro -= 1;
    }

    this.changeReference.detectChanges();
  }

  onClickNavigateToGame() {
    this.routes.navigate(['start']);
    $('#playerModal').modal('hide');
  }

  onClickCallNextModal() {
    $('#resultModal').modal('hide');

    if (this.jogadorFinalScore > this.monstroFinalScore) {
      $('#playerModal').modal('show');
    } else {
      this.routes.navigate(['start']);
    }
  }

  onChangeActiveButtonPlay() {
    if (this.playerName) {
      this.inputPlayGame.nativeElement['disabled'] = false;

      const newPlayer = new Player();
      newPlayer.playerName = this.playerName;
      newPlayer.data = new Date().toLocaleDateString();
      newPlayer.score = this.jogadorFinalScore;
      this.rankingService.addNewPlayerToList(
        this.rankingService.retrievePlayersFromCache(),
        newPlayer
      );
    }
  }

}
