import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Player } from '../models/player';

@Injectable({
  providedIn: 'root'
})
export class RankingService {

  constructor(
    private http: HttpClient
  ) { }

  public getAllPlayers(): Observable<any> {
    return this.http.get<any>('assets/data/players.json');
  }

  public savePlayersOnCache(list: Player[]) {
    sessionStorage.setItem('players', JSON.stringify(list));
  }

  public retrievePlayersFromCache(): Player[] {
    return JSON.parse(sessionStorage.getItem('players'));
  }

  public sortPlayersList(list: Player[]): Player[] {
    list = list.sort((a, b) => {
      if (a.score > b.score) {
        return -1;
      } else if (a.score < b.score) {
        return 1;
      }
      return 0;
    });

    return list;
  }

  public addNewPlayerToList(list: Player[], newPlayer: Player) {
    let ifNewPlayerExists = false;
    let indexOfTheExistent = -1;

    list.forEach((value, i) => {
      if (newPlayer.playerName === value.playerName) {
        ifNewPlayerExists = true;
        indexOfTheExistent = i;
      }
    });

    if (ifNewPlayerExists) {
      list[indexOfTheExistent].score += newPlayer.score;
    } else {
      list.push(newPlayer);
    }

    list = this.sortPlayersList(list);

    sessionStorage.clear();
    this.savePlayersOnCache(list);
  }

}
