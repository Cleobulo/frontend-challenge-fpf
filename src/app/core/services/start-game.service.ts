import { Player } from './../models/player';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StartGameService {

  private currentPlayer: Player;

  constructor() { }

  public savePlayer(player: Player) {
    this.currentPlayer = player;
  }

  public getPlayer(): Player {
    return this.currentPlayer;
  }
}
