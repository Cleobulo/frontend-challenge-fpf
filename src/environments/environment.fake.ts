/**
 * Usa os dados de teste alocados no diretório ´assets´
 */

 export const environment = {
     production: false,
     fakeData: true
 };
